/** 

Project: Project DATA
Developer: Tessa Herzberger
Last Modified Date: 03/22/2018

Subsytem: Sensor Controller
File: servo.h

*/

#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"
#include "stm32f10x.h"

#endif

#ifndef SERVO_H
#define SERVO_H

void clock_init(void);

void PWM_init(void);
void PWM_changeDC(int);
void servo_init(void);
void servo_positive_direction(void);
void servo_negative_direction(void);
void init_delay(void);
void delay(void);
void servo_sweep(void);
void servo_position(int);
void update_servo_pos(void);
void init_global_servo_position(void);
#endif
