/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: sensors.h

*/

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include "stm32f10x.h"

#endif

#ifndef SENSORS_H
#define SENSORS_H

//#include "data.h"

//#define ECHO1_READ			(uint16_t)TIM2->CCR1
//#define ECHO2_READ			(uint16_t)TIM3->CCR1
//#define ECHO3_READ			(uint16_t)TIM3->CCR2
//#define ECHO4_READ			(uint16_t)TIM3->CCR3
//#define ECHO5_READ			(uint16_t)TIM3->CCR4

static int count2 = 0;
static uint32_t current = 0;
static uint32_t last = 0;

void ultrasonic_init(void);

void echo_pulse_in(void);

void get_pulse_width(void);

void tim1_trigger_init(void);


void test_trigger_pulse(void);

void test_echo_pulse(void);
void TIM5_init(void);
void tim_input_init(void);
void tim_output_init(void);

#endif
