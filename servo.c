/** 

	Project: Project DATA
	Developer: Tessa Herzberger
	Creation Date: 03/22/2018

	Subsytem: Safety Controller
	File: servo.c

*/

//Sending most significant byte first, then least significant byte.

#include "servo.h"

#define SERVO_MAX 650
#define SERVO_MAX_THRESH 575
#define SERVO_MIN_THRESH 425
#define SERVO_POSITION_CENTRE 3
#define SERVO_MIN 350
#define START_TIME 0x0
#define	STOP_TIME 0xFFFF
#define SERVO_POSITION_RIGHT 1
#define SERVO_POSITION_LEFT 2
#define SERVO_POSITION_CENTRE 3

//Error Codes
#define ERROR_SERVO_NOT_DETECTED_MAJOR 2009
#define ERROR_SERVO_NOT_DETECTED_MINOR 0

//Servo Address
#define SERVO_POSITION_PID 123

//Servo Position Globals
int global_servo_position;
int global_servo_position_last;

void PWM_changeDC(int duty_cycle)
{
	TIM4->CCR4 = duty_cycle;
}

void servo_init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPBEN;
	GPIOB->CRH = 0xBB; //Initialize to PA8 and PA9 to outputs
	TIM4->CR1 |= TIM_CR1_CEN;
	TIM4->EGR |= TIM_EGR_UG;
	TIM4->CCMR2 |= TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4PE | TIM_CCMR2_OC4FE;
	TIM4->CCER |= TIM_CCER_CC4E;
	TIM4->PSC = 71;
	TIM4->ARR = 20000;
	TIM4->CCR4 = 650;
	TIM4->BDTR |= TIM_BDTR_MOE | TIM_BDTR_OSSI;
	TIM4->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM4->EGR |= TIM_EGR_UG;                                                      
}

void servo_positive_direction(void)
{
	int duty_cycle = SERVO_MIN;
	
	while (duty_cycle <= SERVO_MAX)
	{
		PWM_changeDC(duty_cycle);
		servo_position(duty_cycle);
		duty_cycle++;
		delay();
	}
}	

void servo_negative_direction(void)
{
	int duty_cycle = SERVO_MAX;
	
	while (duty_cycle >= SERVO_MIN)
	{
		PWM_changeDC(duty_cycle);
		servo_position(duty_cycle);
		duty_cycle--;
		delay();
	}
}	

void init_delay(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->CR1 |= TIM_CR1_CEN;
	TIM3->ARR = 0xFFFF; //Auto Reload Register is set to maximum so that the timer is not blocked.
	TIM3->PSC = 0x0;
}

void delay(void)
{	
	TIM3->CNT = START_TIME;
	
	while (TIM3->CNT < STOP_TIME)
	{}
}

void servo_sweep(void)
{
	servo_positive_direction();
	servo_negative_direction();
}

void servo_position(int duty_cycle)
{
	uint16_t position = 0;
	
	if (duty_cycle <= SERVO_MAX && duty_cycle > SERVO_MAX_THRESH)
		position = SERVO_POSITION_RIGHT;
	else if (duty_cycle >= SERVO_MIN && duty_cycle > SERVO_MIN_THRESH)
		position = SERVO_POSITION_LEFT;
	else
		position = SERVO_POSITION_CENTRE;
}

void update_servo_pos(void)
{
	if (global_servo_position == 500 && global_servo_position_last == 350)
	{
		global_servo_position_last = global_servo_position;
		global_servo_position = 650;
	}	
	else if (global_servo_position == 500 && global_servo_position_last == 650)
	{
		global_servo_position_last = global_servo_position;
		global_servo_position = 350;
	}	
	else if (global_servo_position == 650 || global_servo_position == 350)
	{
		global_servo_position_last = global_servo_position;
		global_servo_position = 500;
	}
	
	
//	else
//	{
//		global_servo_position = 500;
//		global_servo_position_last = 350;
//	}

	
	PWM_changeDC(global_servo_position);
}

void init_global_servo_position(void)
{
	global_servo_position = 500;
	global_servo_position_last = 350;
}
